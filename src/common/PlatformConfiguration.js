class PlatformConfiguration {
    constructor () {
      if (!PlatformConfiguration.instance) {
        PlatformConfiguration.instance = this;

        this.initialize();
      }

      return PlatformConfiguration.instance
    }

    initialize() {
        this.chromiumInstalled = false;
        this.chromiumExecutablePath = null;
    }

    setExecutablePathForChromium(path) {
        if (!!path && typeof path === 'string') {
            this.chromiumInstalled = true;
        }
        this.chromiumExecutablePath = path;
    }

    getExecutablePathForChromium() {
        return this.chromiumExecutablePath;
    }

    getBasicScraperOptions() {
        return {
            combineInstallments: false,
            showBrowser: false,
            args: ['--no-sandbox'],
            verbose: true,
            executablePath: this.getExecutablePathForChromium(),
        };
    }

    getRandomString() {
        return (Math.random() + 1).toString(36).substring(2);
    }
    
  }
  
  module.exports = new PlatformConfiguration()