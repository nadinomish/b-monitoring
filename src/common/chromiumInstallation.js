const download = require('download-chromium');
let executablePath = '';
const PlatformConfiguration = require('./PlatformConfiguration');

/// download chromium for puppeteer.
const localDebuger = !process.env.LOCAL_DEBUGGER;
if (localDebuger && false) {
    console.log('LOCAL_DEBUGGER:', process.env.LOCAL_DEBUGGER);
    console.log('Installing:', process.env.LOCAL_DEBUGGER);
    download({ 
        platform: process.platform, 
        revision: '818858', 
        log: false, 
        onProgress: undefined, 
        installPath: `${__dirname}/.local-chromium` })
    .then(execPath => {
        executablePath = execPath;
        PlatformConfiguration.setExecutablePathForChromium(execPath);
        console.log('exec path:', execPath);
        console.log('exec path:', executablePath);
        console.log('exec platform:', process.platform);
    });
} else {

}