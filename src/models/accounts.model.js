const mongoose = require("mongoose");

const Accounts = mongoose.model(
    "Accounts",
    new mongoose.Schema({
        useremail: {
            type: String,
            unique : true,
            required : true
        },
        updateTime: mongoose.Schema.Types.Date,
        startDate: String,
        endDate: String,
        transactions: [{
            type: mongoose.Schema.Types.Mixed,
        }],
        accounts: [{
            type: mongoose.Schema.Types.Mixed,
        }],
    })
);

module.exports = Accounts;