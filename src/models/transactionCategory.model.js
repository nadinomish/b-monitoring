const mongoose = require("mongoose");

const TransactionCategory = mongoose.model(
  "TransactionCategory",
  new mongoose.Schema({
      useremail: String,
      name: String,
      type: String,
      color: String,
      descriptionList: [String],
  }),
);

module.exports = TransactionCategory;