const { CompanyTypes, createScraper } = require('israeli-bank-scrapers-core');
const PlatformConfiguration = require('../common/PlatformConfiguration');
const { executeWorker, WORKERS } = require('../workers/workerService');

const db = require("../models");
const Accounts = db.accounts;

console.log('exec platform:', process.platform);
console.log('LOCAL_DEBUGGER:', process.env.LOCAL_DEBUGGER);

exports.getBAccData = async (req, res) => {
    const scraperList = req.body.scraperList;

    //TODO validate inputs
    const scraperListEnriched = [];
    const accessToken = PlatformConfiguration.getRandomString();
    scraperList.forEach(element => {
        element.options = {
            ...element.options,
            ...PlatformConfiguration.getBasicScraperOptions()
        };
        console.log('list options: ', JSON.stringify(element.options))
        scraperListEnriched.push(
            element
        );
    });

    try {
        executeWorker(WORKERS.scraperWorker, {
            scraperList: scraperListEnriched,
            accessToken: accessToken,
        });
    } catch(error) {
        console.error('Error scraping: ', error);
        res.status(500).send({error, message: 'Error scraping'})
    }

        res.status(200).send({
            message: `started scrapping for ${scraperList.length} sets`, 
            accessToken: accessToken
        });
  };

exports.getBAccSavedData = async (req, res) => {
    const useremail = req.useremail;
    //TODO: make sure only relevant user can access even with right accessToken
    let accounts;

    try {
        console.log('getBAccSavedData querying Accounts by ', useremail);
        accounts = await Accounts.findOne({
            useremail,
        });
    } catch(error) {
        console.error('error querying data by ', useremail);
        res.status(400).send({message: 'could not get data for ' + useremail});
        return;
    }

    if (!accounts) {
        console.log(`executed getBAccSavedData for ${useremail} ... query is not defined`);
        res.status(400).send({
            useremail: useremail,
            updateTime: '',
            accounts: [],
            message: 'Empty query'
        });
        return;
    }

    console.log(`executed getBAccSavedData for ${useremail} ...`);
    console.log(`query keys`, Object.keys(accounts || {}));
    console.log(`query keys len`, Object.keys(accounts || {}).transactions?.length);

    res.status(200).send(accounts);
  }