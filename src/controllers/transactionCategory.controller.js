const TransactionCategory = require("../models/transactionCategory.model");

exports.getAllTransactionCategories = async (req, res) => {
    const useremail = req.useremail;
    let query;

    try {
        query = await TransactionCategory.find({
            useremail
        });
    } catch(error) {
        console.error('error querying data by ');
        res.status(400).send({message: 'could not get data with ' + useremail});
    }

    res.status(200).send(query);
    //a4qj5r766i
}

exports.saveAllTransactionCategories = async (req, res) => {
    const useremail = req.useremail;
    let i = 1;
    console.log(`${i} >> tcParsed: [${JSON.stringify(req.body)}] - [${useremail}]: `, Date());

    const tc = req.body.data;
    let model;
    let tcParsed;
    try {
        tcParsed = JSON.parse(tc);
    } catch(e) {
        tcParsed = tc;
        console.error('error parsing tc', e);
    }
    i++;
    console.log(`${i} >> tcParsed: [${tcParsed}] - [${useremail}]: `, Date());
    i++;
    console.log(`${i} >> ;;;;;;`);
    i++;
    console.log(`${i} >> tc: [${tc}] - [${useremail}]: `, Date());
    try { 
        model = new TransactionCategory({...tc, useremail});
    } catch (e) {
        console.error('Error creating model for TransactionCategory: ', e);
    }
    i++;
    console.log(`${i} >> TC: [${tc}]: `, Date());
    i++;
    console.log(`${i} >> model: [${model}]: `, Date());
    let message = '';
    try {
        await model.save((err) => {
            if (err) {
                message = `Error saving model for TransactionCategory... for: ${useremail}`;
                console.log(message, err);
                res.status(400).send({message});
            } else {
                console.log(`Success saving model for  TransactionCategory for: ${useremail}`);
                res.sendStatus(200);
            }
        });
    } catch(e) {
        message = 'Error for saving TransactionCategory to table';
        console.error(message, e);
        res.status(400).send({message});
    }
    //a4qj5r766i
}