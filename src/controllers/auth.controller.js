require('dotenv').config();
const db = require("../models");
const User = db.user;
const Role = db.role;
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

// var ppp = bcrypt.hashSync("qwertyuiop", 8)
// console.log(`password [qwertyuiop] is [${ppp}]`);

exports.signup = (req, res) => {
    
    const useremail = req.body.useremail;
    const password = req.body.password;

    console.log('body: ', JSON.stringify(req.body));
    console.log(Date(), `email: [${useremail}], password: [${password}]`);
    try {
        const user = new User({
            useremail,
            password: bcrypt.hashSync(password, 8)
        });

        console.log(Date(), `email: [${useremail}], password: [${password}]`);

        user.save((err, user) => {
            if (err) {
                res.status(500).send({ message: err });
                return;
            }
            if (req.body.roles) {
                Role.find(
                    {
                        name: { $in: req.body.roles }
                    },
                    (err, roles) => {
                        if (err) {
                            res.status(500).send({ message: err });
                            return;
                        }
                        user.roles = roles.map(role => role._id);
                        user.save(err => {
                            if (err) {
                                res.status(500).send({ message: err });
                                return;
                            }
                            res.send({ message: "User was registered successfully!" });
                        });
                    }
                );
            } else {
                Role.findOne({ name: "user" }, (err, role) => {
                    if (err) {
                        res.status(500).send({ message: err });
                        return;
                    }
                    user.roles = [role._id];
                    user.save(err => {
                        if (err) {
                            res.status(500).send({ message: err });
                            return;
                        }
                        res.send({ message: "User was registered successfully!" });
                    });
                });
            }
        });
    } catch (error) {
        console.error('Error signing up');
    }
};

exports.signin = (req, res) => {
    const useremail = req.body.useremail;
    const password = req.body.password;

    console.log('body: ', JSON.stringify(req.body));
    console.log(Date(), `email: [${useremail}], password: [${password}]`);

    try {
        User.findOne({
            useremail,
        })
            //.populate("roles", "-__v")
            .exec((err, user) => {
                if (err) {
                    console.log(Date(), 'Error 500');
                    res.status(400).send({ message: err });
                    return;
                }
                if (!user) {
                    console.log(Date(), 'Error 404');
                    return res.status(404).send({ message: "User Not found." });
                }

                console.log(Date(), `signin comparing passwords, user.password: [${user.password}], password: [${password}]`);

                var passwordIsValid = bcrypt.compareSync(
                    password,
                    user.password
                );

                console.log(Date(), `signin comparing passwords .. passwordIsValid: ${passwordIsValid}`);

                if (!passwordIsValid) {
                    console.log(Date(), `sending 401! passwordIsValid: ${passwordIsValid}`);
                    return res.status(401).send({
                        accessToken: null,
                        message: "Invalid Password!"
                    });
                }

                console.log(Date(), ` - reading secret`);
                const token = jwt.sign(
                    { 
                        id: user.id, 
                        useremail: useremail 
                    }, 
                    process.env.SECRET, 
                    {
                        expiresIn: 86400 * 365 // 24 hours * 365 days
                    }
                );
                console.log(Date(), `:]] - token:`, token);
                var authorities = [];

                console.log(Date(), `:]] - authorities:`, authorities);
                const signInObject = {
                    id: user._id,
                    useremail: user.useremail,
                    accessToken: token
                };
                console.log(Date(), `:]] - signInObject:`, signInObject);
                console.log(Date(), `:]] - signInObject.id:`, signInObject.id + '');
                res.status(200).send(signInObject);
            });
    } catch (error) {
        console.error('Error while signing in', error);
    }
};