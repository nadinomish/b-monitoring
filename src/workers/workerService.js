const { Worker } = require('worker_threads')

const runService = (workerName, workerData) => {
    return new Promise((resolve, reject) => {
    
        // import workerExample.js script..
    
        const worker = new Worker(`./src/workers/${workerName}.js`, { workerData });
        worker.on('message', resolve);
        worker.on('error', reject);
        worker.on('exit', (code) => {
            if (code !== 0)
                reject(new Error(`stopped with  ${code} exit code`));
        })
    })
}

const executeWorker = async (workerName, workerData) => {
    console.log('worker service running with ');
    const result = await runService(workerName, workerData)
    console.log('worker service result', result);

    return result;
}

const WORKERS = {
    scraperWorker: 'scraperWorker',
};

module.exports = {
    executeWorker,
    WORKERS
}