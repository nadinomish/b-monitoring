const { CompanyTypes, createScraper } = require('israeli-bank-scrapers-core');
const { WorkerData, parentPort, workerData } = require('worker_threads')
const db = require("../models");

db.mongoose
  //.connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
    .connect(process.env.DB_CONNECTION_STR, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
  })
  .catch(err => {
    console.error("Connection to db error", err);
    process.exit();
  });

const Accounts = db.accounts;
const data = Object.assign({}, workerData|| {}, {startedScrappingAt: Date()})
console.log('doning what worker does with ', data);
data.scraperList = data.scraperList || [];
const identifier = data.identifier;
/**
 * in data: {array of {
 * credentials, options
 * }}
 */

if (!identifier) {
    console.error('No identifier were provided', identifier);
}

let promises = []
data.scraperList.forEach(element => {
    const scraper = createScraper(element.options);
    promises.push(scraper.scrape(element.credentials));
});

let accounts = [];
Promise.allSettled(promises)
    .then((promiseResults) => {
        console.log('scraper worker: promiseResults len: ' + promiseResults?.length)
        promiseResults.forEach((promiseResult, index) => {
            console.log('scraper worker: promiseResult index: ' + index);
            if (promiseResult.status === 'fulfilled') {
                const payload = promiseResult.value;
                console.log(index + '============================ promiseResult payload: ============================ ');
                console.log(index + '============================ promiseResult payload: ============================ ');
                console.log(index + '============================ promiseResult payload: ============================ ');
                console.log('promiseResult payload: ', Object.keys(payload || {}));
                console.log('promiseResult payload: ', payload); 
                console.log(index + '============================ promiseResult payload: ============================ ');
                console.log(index + '============================ promiseResult payload: ============================ ');
                console.log(index + '============================ promiseResult payload: ============================ ');
            
                if (payload.success) {
                    //TODO store data in DB. for data.identifier

                    accounts = accounts.concat(...payload.accounts);
                    console.log(`success results of ${index} for ` + data.scraperList[index].options.companyId + ': ', payload.accounts.length);
                    console.log('total all accounts length: ', accounts.length);
                } else {
                    console.log(`failure results of ${index} for ` + data.scraperList[index].options.companyId + '!');
                }
            } else {
                console.log('scraper worker: promiseResult index: ' + index + ', failed for some reason');
            }
        });

        console.log(`identifier: [${identifier}], accounts: [${accounts.length}]`, Date());
        console.log(`keys keys keys: `, accounts.length ? Object.keys(accounts[0]) : {}, Date());

        return accounts;
    })
    .then(async (accounts) => {
        const accountsModel = new Accounts({
            identifier: identifier,
            accounts: accounts,
            updateTime: new Date(),
        });

        console.log(`identifier: [${identifier}], accounts: [${accounts.length}]`, Date());
        try {
            
            await accountsModel.save((err) => {
                if (err) {
                    console.log(`worker ... error saving to db for: ${identifier}`, err);
                    return;
                } else {
                    console.log(`worker ... success saving to db for: ${identifier}`);
                }
            });
        } catch(e) {
            console.error('Error for saving accounts table', e);
        }
    });




parentPort.postMessage({ message: 'Started working ...' });