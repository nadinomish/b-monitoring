const { 
    verifyToken,
 } = require("../middlewares/authJwt");
 const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();
const controller = require("../controllers/b.account.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post("/api/acc/all", [jsonParser, verifyToken], controller.getBAccData);
  app.post("/api/acc/cached", [jsonParser, verifyToken], controller.getBAccSavedData);
  app.get("/api/acc/cached", [jsonParser, verifyToken], controller.getBAccSavedData);
}