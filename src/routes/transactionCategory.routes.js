const { 
    checkDuplicateOfUseremail,
    checkRolesExisted,
 } = require("../middlewares/verifySignUp");
 const { 
  verifyToken,
} = require("../middlewares/authJwt");
const controller = require("../controllers/transactionCategory.controller");
const bodyParser = require('body-parser');
const { verify } = require("jsonwebtoken");
const jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/cat/all", [jsonParser, verifyToken], controller.getAllTransactionCategories);
  app.post("/api/cat/save", [jsonParser, verifyToken], controller.saveAllTransactionCategories);
};