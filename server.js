require('dotenv').config();
const express = require("express");
require('./src/common/chromiumInstallation');
const cors = require("cors");
const db = require("./src/models");
const Role = db.role;
const dbUtils = require('./src/db.utils');
const app = express();
const DEFAULT_SCRAPERS_TIMEOUT = 120000;

require('./src/routes/auth.routes')(app);
require('./src/routes/user.routes')(app);
require('./src/routes/transactionCategory.routes')(app);
require('./src/routes/b.accounts.routes')(app);

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));
// parse requests of content-type - application/json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

db.mongoose
  //.connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
    .connect(process.env.DB_CONNECTION_STR, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
    dbUtils.initial(Role);
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to b-mon application." });
});
// set port, listen for requests
const PORT = process.env.PORT || 80;
const server = app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

server.timeout = DEFAULT_SCRAPERS_TIMEOUT;

